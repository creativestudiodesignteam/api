<?php

namespace Mpdf;

include('/vendor/mpdf/mpdf/src/Mpdf.php');

$html = "
<fieldset>
        <h1>Relatório Individual</h1>
        <!-- <h3>Informações da ave</h3> -->
        <p class='center sub-titulo'>
            <strong>Informações da ave</strong>
            <br/>
            <img class='center' src='http://198.199.90.249:2048/src/uploads/bird.png' width='100px'>
        </p>
        <p><strong>Anel Direito:</strong> 1</p>
        <p><strong>Anel Esquerdo:</strong> 1</p>
        <p><strong>Anel Ibama:</strong> 1</p>
        <p><strong>Data Nascimento:</strong> 1</p>
        <p><strong>Dias de Vida:</strong> 1</p>
        <p><strong>Sexo:</strong> 1</p>
        <p><strong>Categoria:</strong> 1</p>
        <p><strong>Raça/Grupo:</strong> 1</p>
        <p><strong>Cor:</strong> 1</p>

        <br/><br/><br/>

        <p class='center sub-titulo'>
            <strong>Dados Mãe</strong>
            <br/>
            <img class='center' src='http://198.199.90.249:2048/src/uploads/bird.png' width='100px'>
        </p>
        <p><strong>Anel Direito:</strong> 1</p>
        <p><strong>Anel Esquerdo:</strong> 1</p>
        <p><strong>Categoria:</strong> 1</p>
        <p><strong>Raça/Grupo:</strong> 1</p>
        <p><strong>Cor:</strong> 1</p>

        <br/><br/><br/>

        <p class='center sub-titulo'>
            <strong>Dados Pai</strong>
            <br/>
            <img class='center' src='http://198.199.90.249:2048/src/uploads/bird.png' width='100px'>
        </p>
        <p><strong>Anel Direito:</strong> 1</p>
        <p><strong>Anel Esquerdo:</strong> 1</p>
        <p><strong>Categoria:</strong> 1</p>
        <p><strong>Raça/Grupo:</strong> 1</p>
        <p><strong>Cor:</strong> 1</p>
    </fieldset>
";

require_once __DIR__ . '/vendor/autoload.php';
$mpdf = new mPDF();
$css = file_get_contents("css/estilo.css");
$mpdf->WriteHTML($css, 1);
$mpdf->WriteHTML($html);
$mpdf->Output('filename.pdf','F');
