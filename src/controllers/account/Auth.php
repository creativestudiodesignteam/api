<?php

namespace mthsena\src\controllers\account;

defined('APP_PATH') or exit('No direct script access allowed.');

class Auth {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
	if ($isPost){ http_response_code(200); }
        if($isSigned) {
            $this->post($params);
        } else {
            http_response_code(402);
            exit('402 Not Found. The page you requested does not exist or has been moved. [Controller]');
        }
    }

    private function post($params) {
        $accountRepository = new \mthsena\src\repositories\Accounts();
        $babyRepository = new \mthsena\src\repositories\Babies();
        $eggRepository = new \mthsena\src\repositories\Eggs();
        $email = isset($params['post']['email']) ? strtolower($params['post']['email']) : false;
        $password = isset($params['post']['password']) ? encrypt($email . $params['post']['password']) : false;
        if(!$email || !$password) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $auth = $accountRepository->auth($email, $password);
        if(!$auth) {
            exit(response('danger', 'E-mail ou senha inválidos.'));
        }
        $account = $accountRepository->read($email);
        if(empty($account)) {
            exit(response('danger', 'A conta não foi encontrada.'));
        }
        $account['babies'] = $babyRepository->readAllByAccount($account['id']);;
        $account['eggs'] = $eggRepository->readAllByAccount($account['id']);
        exit(response('success', 'A conta foi obtida com sucesso!', $account));
    }

}
