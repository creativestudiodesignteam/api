<?php

namespace mthsena\src\controllers\account;

defined('APP_PATH') or exit('No direct script access allowed.');

class RecoverySend {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $accountRepository = new \mthsena\src\repositories\Accounts();
        $email = isset($params['post']['email']) ? strtolower($params['post']['email']) : false;
        if(!$email) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $account = $accountRepository->read($email);
        if(empty($account)) {
            exit(response('danger', 'A conta não foi encontrada.'));
        }
        mailer($email, 'Pedido de alteração de senha - Criação Pró', 'Seu código é: ' . $account['password']);
        exit(response('success', 'Enviamos um código para seu email!', $account));
    }

}
