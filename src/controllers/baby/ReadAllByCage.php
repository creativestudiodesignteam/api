<?php

namespace mthsena\src\controllers\baby;

defined('APP_PATH') or exit('No direct script access allowed.');

class ReadAllByCage {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $babyRepository = new \mthsena\src\repositories\Babies();
        $cage = isset($params['post']['cage']) ? $params['post']['cage'] : false;
        if(!$cage) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $babies = $babyRepository->readAllByCage($cage);
        if(empty($babies)) {
            exit(response('danger', 'Os filhotes não foram encontrados.'));
        }
        exit(response('success', 'Os fihotes foram obtidos com sucesso!', $babies));
    }

}
