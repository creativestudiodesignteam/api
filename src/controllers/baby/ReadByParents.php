<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class ReadByParents
{

    public function __construct($params)
    {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if ($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params)
    {
        $birdRepository = new \mthsena\src\repositories\Birds();
        $mother = isset($params['post']['mother']) ? $params['post']['mother'] : false;
        $father = isset($params['post']['father']) ? $params['post']['father'] : false;
        if (!$mother || !$father) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $birds = $birdRepository->readByParents($mother, $father);
        if (empty($birds)) {
            exit(response('danger', 'As aves não foram encontradas.'));
        }
        exit(response('success', 'As aves foram obtidas com sucesso!', $birds));
    }
}
