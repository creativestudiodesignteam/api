<?php

namespace mthsena\src\controllers\baby;

defined('APP_PATH') or exit('No direct script access allowed.');

class Update
{

    public function __construct($params)
    {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if ($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params)
    {
        $babyRepository = new \mthsena\src\repositories\Babies();
        $account = isset($params['post']['account']) ? $params['post']['account'] : false;
        $birthDate = isset($params['post']['birthDate']) ? $params['post']['birthDate'] : false;
        $ringDate = isset($params['post']['ringDate']) ? $params['post']['ringDate'] : false;
        $separateDate = isset($params['post']['separateDate']) ? $params['post']['separateDate'] : false;
        $cage = isset($params['post']['cage']) ? $params['post']['cage'] : false;
        $mother = isset($params['post']['mother']) ? $params['post']['mother'] : -1;
        $father = isset($params['post']['father']) ? $params['post']['father'] : -1;
        $id = isset($params['post']['id']) ? $params['post']['id'] : false;
        if (!$account || !$birthDate || !$ringDate || !$separateDate || !$cage || !$id) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $babyRepository->update($account, $birthDate, $ringDate, $separateDate, $cage, $mother, $father, $id);
        exit(response('success', 'O filhote foi atualizado com sucesso!'));
    }
}
