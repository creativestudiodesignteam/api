<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class DeleteImage {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $birdRepository = new \mthsena\src\repositories\Images();
        $url = isset($params['post']['url']) ? $params['post']['url'] : false;
        if(!$url) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $birdRepository->deleteImage($url);
        exit(response('success', 'A imagem foi excluída com sucesso!'));
    }

}

