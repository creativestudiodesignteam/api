<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class Read {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $birdRepository = new \mthsena\src\repositories\Birds();
        $id = isset($params['post']['id']) ? $params['post']['id'] : false;
        if(!$id) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $bird = $birdRepository->read($id);
        if(empty($bird)) {
            exit(response('danger', 'A ave não foi encontrada.'));
        }
        exit(response('success', 'A ave foi obtida com sucesso!', $bird));
    }

}
