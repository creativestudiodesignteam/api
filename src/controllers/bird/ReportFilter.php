<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class ReportFilter
{

    public function __construct($params)
    {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if ($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params)
    {
        $birdRepository = new \mthsena\src\repositories\Birds();
        $birds = isset($params['post']['birds']) ? $params['post']['birds'] : false;

        $html = "
            <html>
            <head>
            <style>
                table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                }
        
                td,
                th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
                }
        
                tr:nth-child(even) {
                background-color: #dddddd;
                }
                fieldset {
                width: 750px;
                margin: 10px auto;
                color: #444;
                border: 5px solid #ccc;
                font-family: Helvetica;
                padding: 15px;
                }
        
                h1 {
                text-align: center;
                }
        
                h3 {
                text-align: center;
                }
        
                p.sub-titulo {
                font-size: 20px;
                }
        
                .direita {
                text-align: right;
                }
        
                .center {
                text-align: center;
                }
            </style>
            </head>
            <body>
            <fieldset>
                <h1>Relatório das Aves</h1>
                <table>
                <tr>
                    <th>Anel Esquerdo</th>
                    <th>Anel Direito</th>
                    <th>Categoria</th>
                    <th>Raça/Grupo</th>
                    <th>Cor</th>
                    <th>Ano de nascimento</th>
                    <th>Sexo</th>
                    <th>Status</th>
                </tr>
        ";

        foreach ($birds as &$value) {
            $html = $html . "<tr>";

            $html = $html . "<td>" . $value['leftRing'] . "</td>";
            $html = $html . "<td>" . $value['rightRing'] . "</td>";
            $html = $html . "<td>" . $value['category'] . "</td>";
            $html = $html . "<td>" . $value['race'] . "</td>";
            $html = $html . "<td>" . $value['color'] . "</td>";
            $html = $html . "<td>" . $value['yearBirth'] . "</td>";
            $html = $html . "<td>" . $value['gender'] . "</td>";
            $html = $html . "<td>" . $value['status'] . "</td>";

            $html = $html . "</tr>";
        }

        $html = $html . " 
            </table>
            </fieldset>
        </body>
        </html>
        ";


        $birdId = '';

        $reportLink = $birdRepository->reportIndividual($html, $birdId);
        exit(response('success', 'As imagens foram obtidas com sucesso!', $reportLink));
    }
}
