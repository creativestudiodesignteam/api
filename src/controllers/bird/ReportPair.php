<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class ReportPair
{

    public function __construct($params)
    {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if ($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params)
    {
        $birdRepository = new \mthsena\src\repositories\Birds();
        $imageMale = isset($params['post']['imageMale']) ? $params['post']['imageMale'] : false;
        $leftRingMale = isset($params['post']['leftRingMale']) ? $params['post']['leftRingMale'] : false;
        $rightRingMale = isset($params['post']['rightRingMale']) ? $params['post']['rightRingMale'] : false;
        $registryMale = isset($params['post']['registryMale']) ? $params['post']['registryMale'] : false;
        $birthDateMale = isset($params['post']['birthDateMale']) ? $params['post']['birthDateMale'] : false;
        $daysLifeMale = isset($params['post']['daysLifeMale']) ? $params['post']['daysLifeMale'] : false;
        $genderMale = isset($params['post']['genderMale']) ? $params['post']['genderMale'] : false;
        $categoryMale = isset($params['post']['categoryMale']) ? $params['post']['categoryMale'] : false;
        $raceMale = isset($params['post']['raceMale']) ? $params['post']['raceMale'] : false;
        $colorMale = isset($params['post']['colorMale']) ? $params['post']['colorMale'] : false;

        $imageFemale = isset($params['post']['imageFemale']) ? $params['post']['imageFemale'] : false;
        $leftRingFemale = isset($params['post']['leftRingFemale']) ? $params['post']['leftRingFemale'] : false;
        $rightRingFemale = isset($params['post']['rightRingFemale']) ? $params['post']['rightRingFemale'] : false;
        $registryFemale = isset($params['post']['registryFemale']) ? $params['post']['registryFemale'] : false;
        $birthDateFemale = isset($params['post']['birthDateFemale']) ? $params['post']['birthDateFemale'] : false;
        $daysLifeFemale = isset($params['post']['daysLifeFemale']) ? $params['post']['daysLifeFemale'] : false;
        $genderFemale = isset($params['post']['genderFemale']) ? $params['post']['genderFemale'] : false;
        $categoryFemale = isset($params['post']['categoryFemale']) ? $params['post']['categoryFemale'] : false;
        $raceFemale = isset($params['post']['raceFemale']) ? $params['post']['raceFemale'] : false;
        $colorFemale = isset($params['post']['colorFemale']) ? $params['post']['colorFemale'] : false;

        $eggsLaid = isset($params['post']['eggsLaid']) ? $params['post']['eggsLaid'] : false;
        $eggsFull = isset($params['post']['eggsFull']) ? $params['post']['eggsFull'] : false;
        $numberBirths = isset($params['post']['numberBirths']) ? $params['post']['numberBirths'] : false;
        $puppyDeaths = isset($params['post']['puppyDeaths']) ? $params['post']['puppyDeaths'] : false;
        $liveChicks = isset($params['post']['liveChicks']) ? $params['post']['liveChicks'] : false;

        $html = " 
            <fieldset>
                <h1>Relatório Casal</h1>
                <!-- <h3>Informações da ave</h3> -->
                <p class='center sub-titulo'>
                    <strong>Informações do Macho</strong>
                    <br/>
                    <img class='center' src='$imageMale' width='100px'>
                </p>";

        if ($rightRingMale) {
            $html = $html . "<p><strong>Anel Direito:</strong> $rightRingMale</p>";
        }
        if ($rightRingMale) {
            $html = $html . "<p><strong>Anel Esquerdo:</strong> $leftRingMale</p>";
        }
        if ($rightRingMale) {
            $html = $html . "<p><strong>Anel Ibama:</strong> $registryMale</p>";
        }

        $html = $html . "
                <p><strong>Data Nascimento:</strong> $birthDateMale</p>
                <p><strong>Dias de Vida:</strong> $daysLifeMale</p>
                <p><strong>Sexo:</strong> $genderMale</p>
                <p><strong>Categoria:</strong> $categoryMale</p>
                <p><strong>Raça/Grupo:</strong> $raceMale</p>
                <p><strong>Cor:</strong> $colorMale </p>

                <br/><br/><br/>

                <p class='center sub-titulo'>
                    <strong>Informações da Fêmea</strong>
                    <br/>
                    <img class='center' src='$imageFemale' width='100px'>
                </p>";

        if ($rightRingFemale) {
            $html = $html . "<p><strong>Anel Direito:</strong> $rightRingFemale</p>";
        }
        if ($leftRingFemale) {
            $html = $html . "<p><strong>Anel Esquerdo:</strong> $leftRingFemale</p>";
        }
        if ($registryFemale) {
            $html = $html . "<p><strong>Anel Ibama:</strong> $registryFemale</p>";
        }

        $html = $html . "
                <p><strong>Data Nascimento:</strong> $birthDateFemale</p>
                <p><strong>Dias de Vida:</strong> $daysLifeFemale</p>
                <p><strong>Sexo:</strong> $genderFemale</p>
                <p><strong>Categoria:</strong> $categoryFemale</p>
                <p><strong>Raça/Grupo:</strong> $raceFemale</p>
                <p><strong>Cor:</strong> $colorFemale</p>

                <br/><br/><br/>

                <p class='center sub-titulo'>
                    <strong>Ovos e Filhotes</strong>
                    <br/>
                </p>
                <p><strong>Número de ovos botados:</strong> $eggsLaid</p>
                <p><strong>Número de ovos cheios:</strong> $eggsFull</p>
                <p><strong>Número de nascimentos:</strong> $numberBirths</p>
                <p><strong>Número de obitos de filhotes:</strong> $puppyDeaths</p>
                <p><strong>Número de filhotes vivos do casal:</strong> $liveChicks</p>
            </fieldset>
        ";

        $birdId = '';

        $reportLink = $birdRepository->reportIndividual($html, $birdId);
        exit(response('success', 'As imagens foram obtidas com sucesso!', $reportLink));
    }
}
