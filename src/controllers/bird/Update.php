<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class Update {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $birdRepository = new \mthsena\src\repositories\Birds();
        $account = isset($params['post']['account']) ? $params['post']['account'] : false;
        $category = isset($params['post']['category']) ? $params['post']['category'] : false;
        $race = isset($params['post']['race']) ? $params['post']['race'] : false;
        $color = isset($params['post']['color']) ? $params['post']['color'] : false;
        $gender = isset($params['post']['gender']) ? $params['post']['gender'] : false;
        $status = isset($params['post']['status']) ? $params['post']['status'] : false;
        $birthDate = isset($params['post']['birthDate']) ? $params['post']['birthDate'] : false;
        $cage = isset($params['post']['cage']) ? $params['post']['cage'] : -1;
        $rightRing = isset($params['post']['rightRing']) ? $params['post']['rightRing'] : '';
        $leftRing = isset($params['post']['leftRing']) ? $params['post']['leftRing'] : '';
        $registry = isset($params['post']['registry']) ? $params['post']['registry'] : '';
        $name = isset($params['post']['name']) ? $params['post']['name'] : false;
        $mother = isset($params['post']['mother']) ? $params['post']['mother'] : -1;
        $father = isset($params['post']['father']) ? $params['post']['father'] : -1;
        $image = isset($params['post']['image']) ? $params['post']['image'] : host('/src/uploads/bird.png');
        $id = isset($params['post']['id']) ? $params['post']['id'] : false;
        if(!$account || !$category || !$race || !$color || !$gender || !$status || !$birthDate || !$name || !$image || !$id) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $birdRepository->update($account, $category, $race, $color, $gender, $status, $birthDate, $cage, $rightRing, $leftRing, $registry, $name, $mother, $father, $image, $id);
        exit(response('success', 'A ave foi atualizada com sucesso!'));
    }

}
