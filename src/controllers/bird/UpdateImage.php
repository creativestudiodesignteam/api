<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class UpdateImage {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $birdRepository = new \mthsena\src\repositories\Birds();
        $bird = isset($params['post']['bird']) ? $params['post']['bird'] : false;
        $image = isset($params['post']['image']) ? $params['post']['image'] : false;
        if(!$bird || !$image) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $birdRepository->updateImage($image, $bird);
        exit(response('success', 'A imagem foi adicionada com sucesso!'));
    }

}

