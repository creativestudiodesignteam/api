<?php

namespace mthsena\src\controllers\cage;

defined('APP_PATH') or exit('No direct script access allowed.');

class ReadAllByAccount {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $cageRepository = new \mthsena\src\repositories\Cages();
        $account = isset($params['post']['account']) ? $params['post']['account'] : false;
        if(!$account) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $cages = $cageRepository->readAllByAccount($account);
        if(empty($cages)) {
            exit(response('danger', 'As gaiolas não foram encontradas.'));
        }
        exit(response('success', 'As gaiolas foram obtidas com sucesso!', $cages));
    }

}
