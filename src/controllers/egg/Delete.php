<?php

namespace mthsena\src\controllers\egg;

defined('APP_PATH') or exit('No direct script access allowed.');

class Delete {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $eggRepository = new \mthsena\src\repositories\Eggs();
        $id = isset($params['post']['id']) ? $params['post']['id'] : false;
        if(!$id) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $eggRepository->delete($id);
        exit(response('success', 'O ovo foi deletado com sucesso!'));
    }

}
