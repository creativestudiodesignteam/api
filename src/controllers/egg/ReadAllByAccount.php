<?php

namespace mthsena\src\controllers\egg;

defined('APP_PATH') or exit('No direct script access allowed.');

class ReadAllByAccount {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $eggRepository = new \mthsena\src\repositories\Eggs();
        $account = isset($params['post']['account']) ? $params['post']['account'] : false;
        if(!$account) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $eggs = $eggRepository->readAllByAccount($account);
        if(empty($eggs)) {
            exit(response('danger', 'Os ovos não foram encontrados.'));
        }
        exit(response('success', 'Os ovos foram obtidos com sucesso!', $eggs));
    }

}

