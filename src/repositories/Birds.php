<?php

namespace mthsena\src\repositories;

use Exception;

defined('APP_PATH') or exit('No direct script access allowed.');



class Birds
{

    private $table = 'birds';

    public function create($account, $category, $race, $color, $gender, $status, $birthDate, $cage, $rightRing, $leftRing, $registry, $name, $mother, $father, $image)
    {
        $query = 'insert into %s (account, category, race, color, gender, status, birth_date, cage, right_ring, left_ring, registry, name, mother, father, image) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        // $result = databases($query, $this->table, func_get_args());
        try {
            $connection = new \PDO(sprintf('%s:host=%s;dbname=%s;port=%s;charset=utf8;', DB_DRIVER, DB_HOST, DB_NAME, DB_PORT), DB_USERNAME, DB_PASSWORD);
            $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $stmt = $connection->prepare(sprintf($query, $this->table));
            $stmt->execute(func_get_args());
        } catch (Exception $e) {
            exit($e->getMessage());
        }
        return empty($stmt->rowCount()) ? false : $connection->lastInsertId();
    }

    public function read($id)
    {
        $query = 'select * from %s where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetch(\PDO::FETCH_ASSOC);
    }

    public function readAllByAccount($account)
    {
        $query = "select * from %s where account = ? and (status <> 'Vendido' and status <> 'Morto')";
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readAllByAccountPagination($account, $lastId)
    {
        $query = "select * from %s where account = ? and (status <> 'Vendido' and status <> 'Morto') and id > ? order by id limit 10";
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readTotalBirds($account)
    {
        $query = "select count(*) as total from %s where account = ? and (status <> 'Vendido' and status <> 'Morto')";
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readParentsByAccount($account)
    {
        $query = 'select * from %s where account = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readByParents($mother, $father)
    {
        $query = 'select * from %s where mother = ? and father = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readByCage($cage)
    {
        $query = 'select * from %s where cage = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readTotalAccount($account, $id)
    {
        $query = 'select count(*) as total from %s where account = ? and id <> ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readGenderByAccount($account, $gender, $id)
    {
        $query = "select * from %s where account = ? and gender = ? and id <> ? and (status <> 'Vendido' and status <> 'Morto')";
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readAllCategories()
    {
        $query = "select * from categories";
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readAllBreeds($category)
    {
        $query = "select id, name from breeds b inner join categories_breeds c ON c.category_id = ? where c.breed_id = b.id";
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readAllColors($category)
    {
        $query = "select id, name from colors b inner join breeds_colors c ON c.breed_id = ? where c.color_id = b.id";
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readTotalGenderByAccount($account, $gender, $id)
    {
        $query = 'select count(*) as total from %s where account = ? and gender = ? and id <> ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readChildren($birdId1, $birdId2)
    {
        $query = 'select * from %s where father = ? or mother = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function update($account, $category, $race, $color, $gender, $status, $birthDate, $cage, $rightRing, $leftRing, $registry, $name, $mother, $father, $image, $id)
    {
        $query = 'update %s set account = ?, category = ?, race = ?, color = ?, gender = ?, status = ?, birth_date = ?, cage = ?, right_ring = ?, left_ring = ?, registry = ?, name = ?, mother = ?, father = ?, image = ? where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function updateImage($image, $id)
    {
        $query = 'update %s set image = ? where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function delete($id)
    {
        $query = 'delete from %s where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function reportIndividual($html)
    {
        $mpdf = new \Mpdf\Mpdf();
        $css = file_get_contents("css/estilo.css");
        $mpdf->WriteHTML($css, 1);

        $fileName = '/src/uploads/pdf_uplo_' . encrypt(uuid()) . '.' . 'pdf';
        $fileNamePath = APP_PATH . $fileName;

        $mpdf->WriteHTML($html);
        $mpdf->Output($fileNamePath, 'F');
        return host($fileName);
    }
}
