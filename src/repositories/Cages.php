<?php

namespace mthsena\src\repositories;

defined('APP_PATH') or exit('No direct script access allowed.');

class Cages
{

    private $table = 'cages';

    public function create($account, $state, $name)
    {
        $query = 'insert into %s (account, state, name) values (?, ?, ?)';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function read($id)
    {
        $query = 'select * from %s where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetch(\PDO::FETCH_ASSOC);
    }

    public function readAllByAccount($account)
    {
        $query = 'select id, account, state, name, (select count(*) from birds where birds.cage = cages.id) as birds, (select count(*) from babies where babies.cage = cages.id) as babies, (select count(*) from eggs where eggs.cage = cages.id) as eggs from cages where account = ? order by name + 0 asc';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readAllByAccountPagination($lastId, $account)
    {
        $query = 'select id, account, state, name, (select count(*) from birds where birds.cage = cages.id) as birds, (select count(*) from babies where babies.cage = cages.id) as babies, (select count(*) from eggs where eggs.cage = cages.id) as eggs from cages where (name > ? * 1 or name = "" or name REGEXP BINARY "[a-z]" or name REGEXP BINARY "[A-Z]") and account = ? order by name + 0 asc';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readTotalCageActive($account)
    {
        $query = 'select count(*) as totalActive from cages where account = ? and state = "Ativa"';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readTotalCageInactive($account)
    {
        $query = 'select count(*) as totalInactive from cages where account = ? and state = "Inativa"';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readLastName($account)
    {
        $query = 'select max(name + 0) as lastName from cages where account = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readTotalCage($account)
    {
        $query = 'select count(*) as total from cages where account = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function update($account, $state, $name, $id)
    {
        $query = 'update %s set account = ?, state = ?, name = ? where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function delete($id)
    {
        $query = 'delete from %s where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }
}
