<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class ReportIndividual
{

    public function __construct($params)
    {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if ($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params)
    {
        $birdRepository = new \mthsena\src\repositories\Birds();
        $birdId = isset($params['post']['birdId']) ? $params['post']['birdId'] : false;
        $imageBird = isset($params['post']['imageBird']) ? $params['post']['imageBird'] : false;
        $leftRingBird = isset($params['post']['leftRingBird']) ? $params['post']['leftRingBird'] : false;
        $rightRingBird = isset($params['post']['rightRingBird']) ? $params['post']['rightRingBird'] : false;
        $registryBird = isset($params['post']['registryBird']) ? $params['post']['registryBird'] : false;
        $birthDateBird = isset($params['post']['birthDateBird']) ? $params['post']['birthDateBird'] : false;
        $daysLifeBird = isset($params['post']['daysLifeBird']) ? $params['post']['daysLifeBird'] : false;
        $genderBird = isset($params['post']['genderBird']) ? $params['post']['genderBird'] : false;
        $categoryBird = isset($params['post']['categoryBird']) ? $params['post']['categoryBird'] : false;
        $raceBird = isset($params['post']['raceBird']) ? $params['post']['raceBird'] : false;
        $colorBird = isset($params['post']['colorBird']) ? $params['post']['colorBird'] : false;

        $imageBirdMother = isset($params['post']['imageBirdMother']) ? $params['post']['imageBirdMother'] : false;
        $leftRingBirdMother = isset($params['post']['leftRingBirdMother']) ? $params['post']['leftRingBirdMother'] : false;
        $rightRingBirdMother = isset($params['post']['rightRingBirdMother']) ? $params['post']['rightRingBirdMother'] : false;
        $categoryBirdMother = isset($params['post']['categoryBirdMother']) ? $params['post']['categoryBirdMother'] : false;
        $raceBirdMother = isset($params['post']['raceBirdMother']) ? $params['post']['raceBirdMother'] : false;
        $colorBirdMother = isset($params['post']['colorBirdMother']) ? $params['post']['colorBirdMother'] : false;

        $imageBirdFather = isset($params['post']['imageBirdFather']) ? $params['post']['imageBirdFather'] : false;
        $leftRingBirdFather = isset($params['post']['leftRingBirdFather']) ? $params['post']['leftRingBirdFather'] : false;
        $rightRingBirdFather = isset($params['post']['rightRingBirdFather']) ? $params['post']['rightRingBirdFather'] : false;
        $categoryBirdFather = isset($params['post']['categoryBirdFather']) ? $params['post']['categoryBirdFather'] : false;
        $raceBirdFather = isset($params['post']['raceBirdFather']) ? $params['post']['raceBirdFather'] : false;
        $colorBirdFather = isset($params['post']['colorBirdFather']) ? $params['post']['colorBirdFather'] : false;

        $htmlMother = '';
        $htmlFather = '';
        $htmlBird = " 
            <fieldset>
                <h1>Relatório Individual</h1>
                <!-- <h3>Informações da ave</h3> -->
                <p class='center sub-titulo'>
                    <strong>Informações da ave</strong>
                    <br/>
                    <img class='center' src='$imageBird' width='100px'>
                </p>
                <p><strong>Anel Direito:</strong> $rightRingBird</p>
                <p><strong>Anel Esquerdo:</strong> $leftRingBird</p>
                <p><strong>Anel Ibama:</strong> $registryBird</p>
                <p><strong>Data Nascimento:</strong> $birthDateBird</p>
                <p><strong>Dias de Vida:</strong> $daysLifeBird</p>
                <p><strong>Sexo:</strong> $genderBird</p>
                <p><strong>Categoria:</strong> $categoryBird</p>
                <p><strong>Raça/Grupo:</strong> $raceBird</p>
                <p><strong>Cor:</strong> $colorBird</p>

                <br/><br/><br/>
        ";

        if (!$imageBirdMother) {
            $htmlMother = " 
                <p class='center sub-titulo'>
                    <strong>Dados Mãe</strong>
                    <br/>
                </p>
                <p class='center sub-titulo'>
                    <strong>Sem Registros da Mãe</strong>
                </p>

                <br/><br/><br/>
        ";
        } else {
            $htmlMother = " 
                <p class='center sub-titulo'>
                    <strong>Dados Mãe</strong>
                    <br/>
                    <img class='center' src='$imageBirdMother' width='100px'>
                </p>
                <p><strong>Anel Direito:</strong> $rightRingBirdMother</p>
                <p><strong>Anel Esquerdo:</strong> $leftRingBirdMother</p>
                <p><strong>Categoria:</strong> $categoryBirdMother</p>
                <p><strong>Raça/Grupo:</strong> $raceBirdMother</p>
                <p><strong>Cor:</strong> $colorBirdMother</p>

                <br/><br/><br/>
            ";
        }

        if (!$imageBirdFather) {
            $htmlFather = " 
                <p class='center sub-titulo'>
                    <strong>Dados Pai</strong>
                    <br/>
                </p>
                <p class='center sub-titulo'>
                    <strong>Sem Registros do Pai</strong>
                </p>

                <br/><br/><br/>
        ";
        } else {
            $htmlMother = " 
                <p class='center sub-titulo'>
                    <strong>Dados Pai</strong>
                    <br/>
                    <img class='center' src='$imageBirdFather' width='100px'>
                </p>
                <p><strong>Anel Direito:</strong> $rightRingBirdFather</p>
                <p><strong>Anel Esquerdo:</strong> $leftRingBirdFather</p>
                <p><strong>Categoria:</strong> $categoryBirdFather</p>
                <p><strong>Raça/Grupo:</strong> $raceBirdFather</p>
                <p><strong>Cor:</strong> $colorBirdFather</p>
            ";
        }


        $html = $htmlBird . $htmlMother . $htmlFather . "
            </fieldset>
        ";

        $reportLink = $birdRepository->reportIndividual($html, $birdId);
        exit(response('success', 'As imagens foram obtidas com sucesso!', $reportLink));
    }
}
