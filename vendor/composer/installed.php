<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'mthsena/website',
  ),
  'versions' => 
  array (
    'altorouter/altorouter' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '127f6e96998708a31ef32252985bea82e3b03888',
    ),
    'matthiasmullie/minify' => 
    array (
      'pretty_version' => '1.3.63',
      'version' => '1.3.63.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ba1b459828adc13430f4dd6c49dae4950dc4117',
    ),
    'matthiasmullie/path-converter' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e7d13b2c7e2f2268e1424aaed02085518afa02d9',
    ),
    'mpdf/mpdf' => 
    array (
      'pretty_version' => 'v8.0.7',
      'version' => '8.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '7daf07f15334ed59a276bd52131dcca48794cdbd',
    ),
    'mthsena/website' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '969b211f9a51aa1f6c01d1d2aef56d3bd91598e5',
      'replaced' => 
      array (
        0 => '1.10.1',
      ),
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.99',
      'version' => '9.99.99.0',
      'aliases' => 
      array (
      ),
      'reference' => '84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.1.6',
      'version' => '6.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c2796cb1cb99d7717290b48c4e6f32cb6c60b7b3',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'setasign/fpdi' => 
    array (
      'pretty_version' => 'v2.3.4',
      'version' => '2.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '2b5fb811c04f937ef257ef3f798cebeded33c136',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v5.1.0',
      'version' => '5.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e544e24472d4c97b2d11ade7caacd446727c6bf9',
    ),
    'voku/html-min' => 
    array (
      'pretty_version' => '4.4.3',
      'version' => '4.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f7938bb739a64582bcaf413b23b6a2f16defee9',
    ),
    'voku/simple_html_dom' => 
    array (
      'pretty_version' => '4.7.17',
      'version' => '4.7.17.0',
      'aliases' => 
      array (
      ),
      'reference' => '804aca422f1fe0104b1320ca09fd06cd0062782c',
    ),
  ),
);
